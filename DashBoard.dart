import 'package:flutter/material.dart';
import 'package:nokhanyo_george_module_3/screen_1.dart';
import 'package:nokhanyo_george_module_3/profile.dart';
import 'package:nokhanyo_george_module_3/screen_2.dart';
class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text('Dashboard'),
        ) ,
      ),
      body: 
      Center(
          child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.all(20.0),
                  child: new MaterialButton(
                    height: 100.0,
                    minWidth: 100.0,
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.black,
                    child: new Text("Feature Screen 1"),
                    onPressed: () => {
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Screen_1()),
                    )
                    },
                    splashColor: Colors.redAccent,
                  )),
              Padding(
                  padding: EdgeInsets.all(20.0),
                  child: new MaterialButton(
                    height: 100.0,
                    minWidth: 150.0,
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    child: new Text("Feature Screen 2"),
                    onPressed: () => {
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Screen_2()),
                    )
                    },
                    splashColor: Colors.redAccent,
                  )),
                  Padding(
                  padding: EdgeInsets.all(20.0),
                  child: new MaterialButton(
                    height: 100.0,
                    minWidth: 150.0,
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    child: new Text("User Profile"),
                    onPressed: () => {
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Profile()),
                    )
                    },
                    splashColor: Colors.redAccent,
                  )),
            ],
           )] ),)
    );
  }
}

