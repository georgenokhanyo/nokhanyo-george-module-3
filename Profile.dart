mport 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
       title: Center(
          child: Text(User Profile'),
        ) ,

      ),
      body: Column(
        children: [
          SizedBox(
            height: 115,
            width: 115,
            child: Stack(
              fit: StackFit.expand,
              clipBehavior: Clip.none,
              children: const [
                 CircleAvatar(
                  backgroundColor: Colors.amber,
                  backgroundImage: AssetImage("asserts/khanyo.png"),
                )
              ],
            ),
          ),
          
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: TextField(
            obscureText: false,
            decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Username',
  ),
),
          ),
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: TextField(
  obscureText: true,
  decoration: InputDecoration(
    border: OutlineInputBorder(),
    labelText: 'Password',
  ),
),
          )
        ],
      ),
       floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Add your onPressed code here!
        },
        child: const Icon(Icons.edit),
    ));
  }
}

