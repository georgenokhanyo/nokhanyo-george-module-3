import 'package:flutter/material.dart';
import 'package:nokhanyo_george_module_3/dashboard.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            "Login Page",
             textAlign: TextAlign.center,),
        ),
        body: 
        Column(
        children: [
          SizedBox(
            height: 95,
            width: 95,
            child: Stack(
              fit: StackFit.expand,
              clipBehavior: Clip.none,
            ),
          ),
          
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: TextField(
            obscureText: false,
            decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: '    Please enter your Username',
  ),
),
          ),
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: TextField(
            obscureText: true,
            decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Please enter your Password',
  ),
),
          ),
         Padding(
          padding: const EdgeInsets.all(35),
          child: Column(children: [
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Dashboard()),
                    );
              },
             child: const Text('Login')),
            
             ElevatedButton(
              onPressed: () {},
             child: const Text('Register')),
          ])),
          
        ],
      ));
        
  }
}

